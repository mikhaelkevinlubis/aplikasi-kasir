<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

// Route::resource('transaksi','TransaksiController')->middleware('auth');
// Route::get('/transaksi/{transaksi_id}/cetak_pdf', 'TransaksiController@cetakDetailTransaksi')->middleware('auth');
// Route::post('/transaksi/search','TransaksiController@search')->middleware('auth');

// Route::resource('produk','ProdukController')->middleware('auth');
// Route::post('/produk/search','ProdukController@search')->middleware('auth');
// Route::resource('user','UserController')->middleware('auth');


Route::middleware(['auth','role:kasir|admin_kasir'])->group(function(){
    Route::resource('transaksi','TransaksiController');
    Route::get('/transaksi/{transaksi_id}/cetak_pdf', 'TransaksiController@cetakDetailTransaksi');
    Route::post('/transaksi/search','TransaksiController@search');
});

Route::middleware(['auth','role:admin_kasir'])->group(function(){
    Route::resource('produk','ProdukController');
    Route::post('/produk/search','ProdukController@search');
    Route::resource('user','UserController');
});


Route::get('/login', 'LoginController@index')->name('login');
Route::post('/login', 'LoginController@authenticate');
Route::post('/logout', 'LoginController@logout')->middleware('auth');

