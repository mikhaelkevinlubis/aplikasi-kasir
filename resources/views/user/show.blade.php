@extends('layouts.master')
@section('judul','Manage User')
@section('konten')
    <div class="row">
        <div class="col-8">
            <div class="card">
                <div class="card-body">
                    <h1 style="text-align: center"> Daftar User</h1>
                        <table class="table table-bordered" >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                                @forelse ($user as $key=>$item)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->email }}</td>
                                        @if ($item->role == 'kasir')
                                        <td>{{  "Kasir" }}</td>
                                        @endif
                                        @if ($item->role == 'admin_kasir')
                                        <td>{{ "Admin Kasir" }}</td>
                                        @endif
                                        <td>
                                            <form action="/user/{{ $item->id }}" method="post">
                                                <a href="/user/{{ $item->id }}" class="btn btn-sm btn-circle btn-info"><i class="fa fa-search"></i></a>
                                                <a href="/user/{{ $item->id }}/edit" class="btn btn-sm btn-circle btn-warning"><i class="fa fa-pencil-square"></i></a>
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-sm btn-circle btn-danger"><i class="fa fa-trash-o"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5">
                                            <h3 style="text-align: center">
                                                <i >Data User kosong</i>
                                            </h3>
                                        </td>
                                    </tr>
                                @endforelse
                                <tr></tr>
                        </table>
                        <div class="d-flex justify-content-center">
                            {{ $user->links() }}
                        </div>
                            </div>
                        </div>
            
        </div>
        <div class="col-4">
            <div class="card">
                <h1 style="text-align: center">Detail {{ $show->nama_barang }}</h1>
                <div class="card-body">
                   <p>Username : {{ $show->name }}</p>
                   <p>Email : {{ $show->email }}</p>
                   <p>Role : 
                    @if ($show->role == 'kasir')
                    {{  "Kasir" }}
                    @endif
                    @if ($show->role == 'admin_kasir')
                    {{ "Admin Kasir" }}
                    @endif
                   </p>
                </div>
            </div>
        </div>
    </div>
@endsection