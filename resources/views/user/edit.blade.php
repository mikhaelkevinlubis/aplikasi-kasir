@extends('layouts.master')
@section('judul','Manage User')
@section('konten')
    <div class="row">
        <div class="col-8">
            <div class="card">
                <div class="card-body">
                    <h1 style="text-align: center"> Daftar User</h1>
                        <table class="table table-bordered" >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                                @forelse ($user as $key=>$item)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->email }}</td>
                                        @if ($item->role == 'kasir')
                                        <td>{{  "Kasir" }}</td>
                                        @endif
                                        @if ($item->role == 'admin_kasir')
                                        <td>{{ "Admin Kasir" }}</td>
                                        @endif
                                        <td>
                                            <form action="/user/{{ $item->id }}" method="post">
                                                <a href="/user/{{ $item->id }}" class="btn btn-sm btn-circle btn-info"><i class="fa fa-search"></i></a>
                                                <a href="/user/{{ $item->id }}/edit" class="btn btn-sm btn-circle btn-warning"><i class="fa fa-pencil-square"></i></a>
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-sm btn-circle btn-danger"><i class="fa fa-trash-o"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5">
                                            <h3 style="text-align: center">
                                                <i >Data User kosong</i>
                                            </h3>
                                        </td>
                                    </tr>
                                @endforelse
                                <tr></tr>
                        </table>
                            </div>
                        </div>
            
        </div>
        <div class="col-4">
            <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#tambahUser" class="d-block card-header py-3" data-toggle="collapse"
                    role="button" aria-expanded="true" aria-controls="tambahUser">
                    <h6 class="m-0 font-weight-bold text-primary">Edit User</h6>
                </a>
                <!-- Card Content - Collapse -->
                <div class="collapse show" id="tambahUser">
                    <div class="card-body">
                        <form action="/user/{{ $edit->id }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="username">Username : </label>
                                <input type="text" class="form-control bg-light border-1 small @error('name') is-invalid @enderror" name="name" id="name" value="{{ $edit->name }}">
                                @error('name')
                                <p class="invalid-feedback">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="email">E-Mail :</label> 
                                <input type="email" class="form-control bg-light border-1 small @error('email') is-invalid @enderror" name="email" id="email" value="{{ $edit->email }}">
                                @error('email')
                                <p class="invalid-feedback">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="role">Role</label>
                                <select name="role" class="form-control bg-light border-1 small">
                                    @if ($edit->role == 'kasir')
                                    <option value="kasir" selected>Kasir</option>
                                    <option value="admin_kasir">Admin Kasir</option>
                                    @else
                                    <option value="kasir">Kasir</option>
                                    <option value="admin_kasir" selected>Admin Kasir</option>
                                    @endif
                                </select>
                            </div>
                            <div class="d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
@endsection