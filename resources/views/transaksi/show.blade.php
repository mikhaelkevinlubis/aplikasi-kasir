@extends('layouts.master')
@section('judul','Detail Transaksi')
@section('konten')
    <div class="row">
        <div class="col-8">
            <div class="card">
                <h2 style="text-align: center" class="mt-3">Daftar Transaksi</h2>
                <div class="card-body">
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th>Id Transaksi</th>
                            <th>Waktu Transaksi</th>
                            <th>Total Harga</th>
                            <th>Action</th>
                        </tr>
                        @forelse ($transaksi as $record)
                            <tr>
                                <td>
                                    {{ date('Ymd') . $record->id . 'JCC' }}
                                </td>
                                <td>
                                    {{ strftime("%A, %d %B %Y %H:%M", strtotime(date($record->created_at))) }}
                                </td>
                                <td style="text-align: end">
                                    {{'Rp. ' . number_format($record->total_harga) }}
                                </td>
                                <td>
                                    <a href="/transaksi/{{ $record->id }}" class="btn btn-sm btn-circle btn-info"><i class="fa fa-search"></i></a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">
                                    <h3 style="text-align: center"><i>Belum ada transaksi</i></h3>
                                </td>
                            </tr>
                        @endforelse
                    </table>
                    <div class="d-flex justify-content-center">
                        {{ $transaksi->links() }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card">
                <h2 style="text-align: center" class="mt-3">Detail Transaksi</h2>
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            
                        </div>
                        <div class="col-6 d-flex justify-content-end mb-5">
                            {{ 'ID: ' . date('Ymd') . $transaksi_pembelian->id . 'JCC' }} <br>
                            {{ strftime("%A, %d %B %Y", strtotime(date($transaksi_pembelian->created_at))) }}
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-6 d-flex justify-content-start">
                        @foreach ($nama_barang as $value)
                            {{ $value->master_barangs->nama_barang . ' - ' . $value->jumlah . ' pcs' }}
                            <br>
                        @endforeach
                        </div>
                        <div class="col-6 d-flex justify-content-end">
                            @foreach ($nama_barang as $value)
                            {{ 'Rp. ' . number_format($value->sub_total) }}
                            <br>
                            @endforeach
                            
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-6 d-flex justify-content-end">
                            Total: 
                        </div>
                        <div class="col-6 d-flex justify-content-end">
                            {{ 'Rp. ' . number_format($transaksi_pembelian->total_harga) }}
                        </div>
                    </div>
                    <div class="row mt-5 d-flex justify-content-center">
                        <a href="/transaksi/{{ $transaksi_pembelian->id }}/cetak_pdf" class="btn btn-danger"><i class="fa fa-print" aria-hidden="true"></i> Print</a>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
@endsection