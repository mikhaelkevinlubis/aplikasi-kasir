<!DOCTYPE html>
<html>
<head>
	<title>Cetak Detail Transaksi </title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <div style="font-size: 7px !important;">
        <table style="width:100%">
            <thead>
                <tr>
                    <th colspan="4" style="text-align: center"><b>STRUK TRANSAKSI</b></th>
                </tr>
            </thead>
        </table>
    </div>
	<div style="font-size: 5px !important;">
        <table style="width:100%" class="mt-2">
            <thead>
                <tr>
                    <th colspan="4" style="text-align: right"><b style="text-align: right">{{ 'ID: ' . date('Ymd') . $id . 'JCC' }}</b></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="3"></td>
                    <td style="text-align: right"><b style="text-align: right font">{{ strftime("%d %B %Y", strtotime(date($tanggal))) }}</b></td>
                </tr>
            </tbody>
        </table>
        <table style="width:100%"  class="mt-3">
            <thead>
                <tr>
                    <th>Nama Barang</th>
                    <th>Qty</th>
                    <th></th>
                    <th style="text-align: right">Sub Total</th>
                </tr>
            </thead>
            <tbody>
                @for ($i = 0; $i < count($subTotal); $i++)
            <tr>
                <td style="text-align: left">{{ $namaBarang[$i] }}</td>
                <td style="text-align: center">{{ $qty[$i] }}</td>
                <td>. . . . . . .</td>
                <td style="text-align: right">{{'Rp. ' . number_format($subTotal[$i]) }}</td>
            </tr>
            @endfor
            
            <tr>
                <td colspan="2"></td>
                <td style="text-align: right"><b>Total:</b></td>
                <td style="text-align: right"><b>{{ 'Rp. ' . number_format($totalHarga) }}</b></td>
            </tr>
            </tbody>
        </table>    
        <hr>  
        <table style="width:100%" cellpadding="5" class="mt-2">
            <thead>
                <tr>
                    <th colspan="4" style="text-align: center">
                        <i>Terimakasih telah berbelanja ditoko kami, semoga anda puas dengan pelayanan kami.</i>
                    </th>
                </tr>
            </thead>
        </table>
	</div>
</body>
<footer>
    
</footer>
</html>