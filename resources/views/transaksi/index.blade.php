@extends('layouts.master')
@section('judul','Daftar Transaksi')
@push('script_head')
<link rel="stylesheet" href="//cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
@endpush
@section('konten')
    <div class="row">
        <div class="col-8">
            <div class="card">
                <h2 style="text-align: center" class="mt-3">Daftar Transaksi</h2>
                <div class="card-body">
                    <table class="table table-bordered" id="dataTableTransaksi">
                        <thead>
                            <tr>
                                <th>Id Transaksi</th>
                                <th>Waktu Transaksi</th>
                                <th>Total Harga</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (Auth::user() && Auth::user()->role == 'kasir' )
                            @forelse ($transaksi_kasir as $record)
                                <tr>
                                    <td>
                                        {{ date('Ymd') . $record->id . 'JCC' }}
                                    </td>
                                    <td>
                                        {{ strftime("%A, %d %B %Y %H:%M", strtotime(date($record->created_at))) }}
                                    </td>
                                    <td style="text-align: end">
                                        @currency($record->total_harga)
                                    </td>
                                    <td>
                                        <a href="/transaksi/{{ $record->id }}" class="btn btn-sm btn-circle btn-info"><i class="fa fa-search"></i></a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="4">
                                        <h3 style="text-align: center"><i>Belum ada transaksi hari ini</i></h3>
                                    </td>
                                </tr>
                            @endforelse  
                            @else
                            @forelse ($transaksi as $record)
                                <tr>
                                    <td>
                                        {{ date('Ymd') . $record->id . 'JCC' }}
                                    </td>
                                    <td>
                                        {{ strftime("%A, %d %B %Y %H:%M", strtotime(date($record->created_at))) }}
                                    </td>
                                    <td style="text-align: end">
                                        @currency($record->total_harga)
                                    </td>
                                    <td>
                                        <a href="/transaksi/{{ $record->id }}" class="btn btn-sm btn-circle btn-info"><i class="fa fa-search"></i></a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="4">
                                        <h3 style="text-align: center"><i>Belum ada transaksi</i></h3>
                                    </td>
                                </tr>
                            @endforelse
                            @endif
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-center">
                    </div>
                </div>
            </div>
        </div>
        @if (Auth::user() && Auth::user()->role == "admin_kasir")
        <div class="col-4">
            <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#tambahUser" class="d-block card-header py-3 collapsed" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="tambahUser">
                    <h6 class="m-0 font-weight-bold text-primary">Advance Search</h6>
                </a>
                <!-- Card Content - Collapse -->
                <div class="collapse" id="tambahUser">
                    <div class="card-body">
                        <form action="/transaksi/search" method="POST">
                        @csrf
                        <div class="row mt-2">
                            <div class="col-4">
                                <p>Tanggal Transaksi: </p>
                            </div>
                            <div class="col-8">
                                <input type="date" name="filter_tanggal" class="form-control bg-light border-1">
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-4">
                                <p>Total Harga Min/Max: </p>
                            </div>
                            <div class="col-4">
                                <input type="number" name="filter_harga_min" class="form-control bg-light border-1" placeholder="Min" min="0">
                            </div>
                            <div class="col-4">
                                <input type="number" name="filter_harga_max" class="form-control bg-light border-1" placeholder="Max" min="0">
                            </div>
                        </div>
                        <div class="row">
                            {{-- <div class="col-4">
                                <p>Barang: </p>
                            </div>
                            <div class="col-4">
                                <select name="filter_barang_1" class="form-control bg-light border-1">
                                    <option selected disabled>Pilih Barang</option>
                                    @foreach ($barang as $item)
                                    <option value="{{ $item->id }}">{{ $item->nama_barang }}</option>
                                    @endforeach
                                </select>
                            </div> --}}
                            {{-- <div class="col-4">
                                <select name="filter_barang_2" class="form-control bg-light border-1">
                                    <option value="" selected disabled>Pilih Barang</option>
                                    <@foreach ($barang as $item)
                                    <option value="{{ $item->id }}">{{ $item->nama_barang }}</option>
                                    @endforeach
                                </select>
                            </div> --}}
                        </div>
                        <div class="d-flex justify-content-end mt-5">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            
        </div>
        @endif
        
    </div>
@endsection
@push('script')
<script src="//cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready( function () {
        $('#dataTableTransaksi').DataTable();
    } );
</script>
@endpush