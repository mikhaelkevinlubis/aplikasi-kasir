@extends('layouts.master')
@section('judul','Catat Transaksi')
@push('script_head')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
@endpush
@section('konten')
<h1 style="text-align: center" class="mb-3">Form Transaksi</h1>

<div class="row">
    <div class="col-2"></div>
    <div class="card col-8">
        <div class="card-body">
            <form action="/transaksi" method="POST">
                @csrf
                <div class="form-group">
                    @if ($id == 0)
                    @php
                        $id += 1;
                    @endphp
                    @else
                    @php
                        $id += 1;
                    @endphp
                    @endif
                    <label>No. Transaksi : {{ date('Ymd') . $id . 'JCC' }}</label>
                  </div>
                  <table class="table">
                      <thead>
                          <tr>
                              <th>Nama Barang</th>
                              <th>Qty</th>
                              <th colspan="2">Sub Total</th>
                          </tr>
                      </thead>
                      <tbody id="tbody">
                          <tr>
                              <input type="number" name="transaksi_pembelian_id" value="{{ $id }}" hidden> 
                              <td>
                                  <select class="form-control selectpicker" name="master_barang_id[]" data-live-search="true">
                                    @foreach ($barang as $item)   
                                      <option value="{{ $item->id }}" data-price="{{ $item->harga_satuan }}">
                                          {{ $item->nama_barang }} - @Rp {{ number_format($item->harga_satuan) }}
                                        </option>
                                    @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="number" class="form-control form-quantity" min="1" name="jumlah[]" required>
                                    </p>
                                </td>
                                <td>
                                    <input type="number" class="form-control subtotal" name="sub_total[]" readonly required>
                                </td>
                                <td>
                                    <a href="#"class="btn-circle btn-sm btn-primary addRow">+</a>
                                </td>
                            </tr>
                        </tbody>
                    </table> 
                    <div class="col-2">
                        <h3>Total: </h3>{{-- input ke tabel transaksi_pembelian | field: total_harga --}}
                        <input type="number" class="form-control border-0 w-100 @error('total_harga') is-invalid @enderror"  name="total_harga" readonly>
                        @error('total_harga')
                            <p class="invalid-feedback">
                                {{ $message }}
                            </p>
                        @enderror
                    </div>
                    
                    <div class="d-flex justify-content-end">
                        <button type="submit" name="action" value="print" class="btn btn-danger mr-1"><i class="fa fa-print" aria-hidden="true"></i>Print</button>
                        <button type="submit" name="action" value="save" class="btn btn-primary">Submit</button>
                    </div>
                
              </form>
              
        </div>
    <div class="col-2"></div>
</div>
</div>
@endsection
@push('script')
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
    <script>
        $('.addRow').on('click', function(){
            addRow();
            $('.selectpicker').selectpicker('refresh');
        });
        
        function addRow(){
            var tr ='<tr>'+
                    '<input type="number" name="transaksi_pembelian_id[]" value="{{ $id }}" hidden>'+
                    '<td>'+
                        '<select class="form-control selectpicker" name="master_barang_id[]" data-live-search="true">'+
                        '@foreach ($barang as $key=>$item)'+   
                            '<option value="{{ $item->id }}" data-price="{{ $item->harga_satuan }}">'+
                                '{{ $item->nama_barang }} - @Rp. {{ $item->harga_satuan }}'+
                            '</option>'+
                        '@endforeach'+
                        '</select>'+
                    '</td>'+
                    '<td>'+
                        '<input type="number" class="form-control form-quantity @error('jumlah') is-invalid @enderror" min="1" name="jumlah[]" required>'+
                        '@error('jumlah')'+
                                    '<p class="invalid-feedback">'+
                                        '{{ $message }}'+
                                    '@enderror'+
                    '</td>'+
                    '<td>'+
                        '<input type="number" class="form-control subtotal @error('sub_total[]') @enderror"  name="sub_total[]" readonly required>'+
                        '@error('sub_total[]')'+
                        '<p class="invalid-feedback>{{ $message }}</p>'+
                        '@enderror'+
                    '</td>'+
                    '<td>'+
                        '<a href="#"class="btn-circle btn-sm btn-danger removeRow">-</a>'+
                    '</td>'+
            '</tr>';
            $('tbody').append(tr);
        };

        $('tbody').on('click','.removeRow',function(){
            $(this).parent().parent().remove();
        });

        $(document).on('keyup', '.form-quantity', function(e) {
            let parent = $(this).closest('tr');
            let select = parent.find('select').find(':selected');
            let grandTotal = 0;
            parent.find('.subtotal').val(select.data('price') * e.target.value);

            $('tbody tr').each(function() {
                if($(this).find('.subtotal').val())
                    grandTotal += parseInt($(this).find('.subtotal').val())
            })
            $('input[name="total_harga"]').val(grandTotal)
        })

        $(document).on('change', '.selectpicker', function(e) {
            let parent = $(this).closest('tr');
            let qty = parent.find('.form-quantity').val();
            let grandTotal = 0;
            parent.find('.subtotal').val($(this).find(':selected').data('price') * qty);

            $('tbody tr').each(function() {
                if($(this).find('.subtotal').val())
                    grandTotal += parseInt($(this).find('.subtotal').val())
            })
            $('input[name="total_harga"]').val(grandTotal)
        })

    </script>
@endpush