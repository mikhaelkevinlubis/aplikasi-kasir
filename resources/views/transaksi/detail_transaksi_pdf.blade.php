<!DOCTYPE html>
<html>
<head>
	<title>Cetak Detail Transaksi </title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<div class="container-fluid">
        <h2 style="text-align: center" class="mt-3">Detail Transaksi</h2>
        <table class="table table-borderless mt-5">
            <tr style="border-top: none">
                <th style="text-align: left"> 
                    APLIKASI KASIR <br>
                    JABAR CODING CAMP   
                </th>
                <th style="text-align: right"> 
                    {{ 'ID: ' . date('Ymd') . $transaksi_pembelian->id . 'JCC' }} <br>
                    {{ strftime("%A, %d %B %Y", strtotime(date($transaksi_pembelian->created_at))) }} 
                </th>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            @foreach ($nama_barang as $value)
            <tr>
                <td>
                    {{ $value->master_barangs->nama_barang . ' - ' . $value->jumlah . ' pcs' }}
                    <hr>
                </td>
                <td style="text-align: right">
                    {{ 'Rp. ' . number_format($value->sub_total) }}
                    <hr>
                </td>
            </tr>
            @endforeach
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td style="text-align: right">Total: </td>
                <td style="text-align: right">{{ 'Rp. ' . number_format($transaksi_pembelian->total_harga) }}</td>
            </tr>
        </table>
	</div>
</body>
</html>