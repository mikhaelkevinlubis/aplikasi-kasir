@extends('layouts.master')
@section('judul','Daftar Transaksi')
@section('konten')
    <div class="row">
        <div class="col-2"></div>
        <div class="col-8">
            <div class="card">
                <h2 style="text-align: center" class="mt-3">Daftar Transaksi</h2>
                <div class="card-body">
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th>Id Transaksi</th>
                            <th>Waktu Transaksi</th>
                            <th>Total Harga</th>
                            <th>Action</th>
                        </tr>
                        @forelse ($search as $record)
                            <tr>
                                <td>
                                    {{ date('Ymd') . $record->id . 'JCC' }}
                                </td>
                                <td>
                                    {{ strftime("%A, %d %B %Y %H:%M", strtotime(date($record->created_at))) }}
                                </td>
                                <td style="text-align: end">
                                    {{'Rp. ' . number_format($record->total_harga) }}
                                </td>
                                <td>
                                    <a href="/transaksi/{{ $record->id }}" class="btn btn-sm btn-circle btn-info"><i class="fa fa-search"></i></a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">
                                    <h3 style="text-align: center"><i>Transaksi tidak ditemukan</i></h3>
                                </td>
                            </tr>
                        @endforelse
                    </table>
                    <div class="d-flex justify-content-center">
                        <a href="/transaksi" class="btn btn-primary">Back</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-2"></div>
    </div>
@endsection