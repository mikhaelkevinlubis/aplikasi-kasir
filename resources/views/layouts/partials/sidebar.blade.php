<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-shopping-cart"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Aplikasi Kasir</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">


    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Transaksi
    </div>

    
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#manageTransaksi"
            aria-expanded="true" aria-controls="manageTransaksi">
            <i class="fas fa-cart-arrow-down"></i>
            <span>Transaksi</span>
        </a>
        <div id="manageTransaksi" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Kelola Transaksi</h6>
                <a class="collapse-item" href="/transaksi">Daftar Transaksi</a>
                <a class="collapse-item" href="/transaksi/create">Tambah Transaksi</a>
            </div>
        </div>
    </li>
    
    @if (Auth::user() && Auth::user()->role == "admin_kasir")
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <div class="sidebar-heading">
        Barang
    </div>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#manageBarang"
            aria-expanded="true" aria-controls="manageBarang">
            <i class="fas fa fa-list-alt"></i>
            <span>Barang</span>
        </a>
        <div id="manageBarang" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Kelola Barang</h6>
                <a class="collapse-item" href="/produk">Daftar Barang</a>
            </div>
        </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Transaksi
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#manageUser"
            aria-expanded="true" aria-controls="manageUser">
            <i class="fas fa fa-user"></i>
            <span>User</span>
        </a>
        <div id="manageUser" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Kelola Transaksi</h6>
                <a class="collapse-item" href="/user">Daftar User</a>
            </div>
        </div>
    </li>
    @endif

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

    


</ul>
<!-- End of Sidebar -->