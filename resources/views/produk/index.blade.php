@extends('layouts.master')
@section('judul','Produk')
@section('konten')
<div class="row">
    <div class="card col-8">
    <h1 style="text-align: center" class="mt-3">Daftar Barang</h1>
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama Barang</th>
                            <th>Harga Satuan</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($barang as $key=>$item)
                        <tr id="filterSearch">
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $item->nama_barang }}</td>
                                <td>{{ $item->harga_satuan }}</td>
                                <td>
                                    <form action="/produk/{{ $item->id }}" method="post">
                                        <a href="/produk/{{ $item->id }}" class="btn btn-sm btn-circle btn-info"><i class="fa fa-search"></i></a>
                                        <a href="/produk/{{ $item->id }}/edit" class="btn btn-sm btn-circle btn-warning"><i class="fa fa-pencil-square"></i></a>
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-sm btn-circle btn-danger"><i class="fa fa-trash-o"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4"><i>Data barang kosong.</i></td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                <div class="d-flex justify-content-center">
                    {{ $barang->links() }}
                </div>
            </div>
    </div>
    <div class="col-4">
        <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#tambahBarang" class="d-block card-header py-3" data-toggle="collapse"
                role="button" aria-expanded="true" aria-controls="tambahBarang">
                <h6 class="m-0 font-weight-bold text-primary">Tambah Barang</h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse show" id="tambahBarang">
                <div class="card-body">
                    <form action="/produk" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="nama_barang">Nama Barang: </label>
                            <input type="text" class="form-control bg-light border-1 small @error('nama_barang') is-invalid @enderror"  name="nama_barang" id="nama_barang">
                            @error('nama_barang')
                                <p class="invalid-feedback">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="harga_satuan">Harga Satuan: </label>
                            <input type="number" class="form-control bg-light border-1 small @error('harga_satuan') is-invalid @enderror" name="harga_satuan" id="harga_satuan" min="0">
                            @error('harga_satuan')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#filterBarang" class="d-block card-header py-3 collapsed" data-toggle="collapse"
                role="button" aria-expanded="false" aria-controls="filterBarang">
                <h6 class="m-0 font-weight-bold text-primary">Filter Barang <i class="fa fa-search"></i></h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse" id="filterBarang">
                <div class="card-body">
                    <form action="/produk/search" method="post">
                        @csrf
                        <div class="row">
                            <div class="form-group col-6">
                                <label for="filterNama">Nama Barang: </label>
                                <input type="text" class="form-control bg-light border-1 small" name="filter_nama_barang" id="filterNama">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-4">
                                <label for="filterHarga">Harga: </label>
                                <input type="number" name="filter_harga" id="filterHarga" class="form-control bg-light border-1 small @error('filter_harga') is-invalid @enderror" min="0">
                                @error('filter_harga')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group col-4">
                                <label for="filterHargaMin">Harga Min: </label>
                                <input type="number" name="filter_harga_min" id="filterHargaMin" class="form-control bg-light border-1 small @error('filter_harga_min') is-invalid @enderror" min="0">
                                @error('filter_harga_min')
                                <p class="invalid-feedback">
                                    {{ $message }}
                                </p>
                                @enderror
                            </div>
                            <div class="form-group col-4">
                                <label for="filterHargaMax">Harga Max: </label>
                                <input type="number" name="filter_harga_max" id="filterHargaMax"  class="form-control bg-light border-1 small @error('filter_harga_max') is-invalid @enderror" min="0">
                                @error('filter_harga_max')
                                <p class="invalid-feedback">
                                    {{ $message }}
                                </p>
                                @enderror
                            </div>
                        </div>
                        <div class="d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
    </div>
</div>
@endsection