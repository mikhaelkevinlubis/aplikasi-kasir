@extends('layouts.master')
@section('judul','Detail Produk')
@section('konten')
<div class="row">
    <div class="card col-8">
    <h1 style="text-align: center">Daftar Barang</h1>
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama Barang</th>
                            <th>Harga Satuan</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($barang as $key=>$item)
                        <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $item->nama_barang }}</td>
                                <td>{{ $item->harga_satuan }}</td>
                                <td>
                                    <form action="/produk/{{ $item->id }}" method="post">
                                        <a href="/produk/{{ $item->id }}" class="btn btn-sm btn-circle btn-info"><i class="fa fa-search"></i></a>
                                        <a href="/produk/{{ $item->id }}/edit" class="btn btn-sm btn-circle btn-warning"><i class="fa fa-pencil-square"></i></a>
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-sm btn-circle btn-danger"><i class="fa fa-trash-o"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="d-flex justify-content-center">
                    {{ $barang->links() }}
                </div>
            </div>
    </div>
    <div class="col-4">
        <div class="card">
            <h1 style="text-align: center">Detail {{ $show->nama_barang }}</h1>
            <div class="card-body">
               <p> Nama Barang : {{ $show->nama_barang }}</p>
               <p>Harga Satuan : {{ $show->harga_satuan }}</p>
               <p>Jumlah Terjual : {{ $count }} pcs</p>
            </div>
        </div>
    </div>
</div>
@endsection