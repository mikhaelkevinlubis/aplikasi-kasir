@extends('layouts.master')
@section('judul','Edit Produk')
@section('konten')
<div class="row">
    <div class="card col-8">
    <h1 style="text-align: center">Daftar Barang</h1>
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama Barang</th>
                            <th>Harga Satuan</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($barang as $key=>$item)
                        <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $item->nama_barang }}</td>
                                <td>{{ $item->harga_satuan }}</td>
                                <td>
                                    <form action="/produk/{{ $item->id }}" method="post">
                                        <a href="/produk/{{ $item->id }}" class="btn btn-sm btn-circle btn-info"><i class="fa fa-search"></i></a>
                                        <a href="/produk/{{ $item->id }}/edit" class="btn btn-sm btn-circle btn-warning"><i class="fa fa-pencil-square"></i></a>
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-sm btn-circle btn-danger"><i class="fa fa-trash-o"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="d-flex justify-content-center">
                    {{ $barang->links() }}
                </div>
            </div>
    </div>
    <div class="col-4">
        <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#editBarang" class="d-block card-header py-3" data-toggle="collapse"
                role="button" aria-expanded="true" aria-controls="editBarang">
                <h6 class="m-0 font-weight-bold text-primary"> Edit Barang</h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse show" id="editBarang">
                <div class="card-body">
                    <form action="/produk/{{ $edit->id }}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="nama_barang">Nama Barang: </label>
                            <input type="text" class="form-control bg-light border-1 small @error('nama_barang') is-invalid @enderror" name="nama_barang" id="nama_barang" value="{{ $edit->nama_barang }}">
                            @error('nama_barang')
                                <p class="invalid-feedback">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="harga_satuan">Harga Satuan: </label>
                            <input type="number" class="form-control bg-light border-1 small @error('harga_satuan') is-invalid @enderror" name="harga_satuan" id="harga_satuan" value="{{ $edit->harga_satuan }}" min="0">
                            @error('harga_satuan')
                                <p class="invalid-feedback">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="form-group d-flex justify-content-end">
                            <button type="submit"  class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection