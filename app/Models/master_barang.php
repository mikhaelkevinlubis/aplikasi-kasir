<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class master_barang extends Model
{
    protected $fillable = ['nama_barang','harga_satuan'];
    public $timestaps = true;

    public function transaksi_pembelian_barangs(){
        return $this->hasMany('App\Models\transaksi_pembelian_barang');
    }
}
