<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class transaksi_pembelian_barang extends Model
{
    protected $fillable = ['jumlah','harga_satuan','transaksi_pembelian_id','master_barang_id'];

    public function master_barangs(){
        return $this->belongsTo('App\Models\master_barang','master_barang_id');
    }

    public function transaksi_pembelians(){
        return $this->belongsTo('App\Models\transaksi_pembelian','transaksi_pembelian_id');
    }
}
