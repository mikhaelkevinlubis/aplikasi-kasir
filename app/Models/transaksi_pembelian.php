<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class transaksi_pembelian extends Model
{
    protected $fillable =['total_harga'];
    public $timestamps = true;

    public function transaksi_pembelian_barangs(){
        return $this->hasMany('App\Models\transaksi_pembelian_barang');
    }
}
