<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::where('name', '!=' , Auth::user()->name)->get();
        return view('user.index',compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'name' => 'required|alpha_dash|unique:users,name',
            'email' => 'required|unique:users,email',
            'password' => 'required',
            'role' => 'required'
        ],
        [
            'name.required' => 'Username harus diisi!',
            'name.alpha_dash' => 'Username mengandung simbol yang dilarang',
            'name.unique' => 'Username tidak tersedia!',
            'email.required' => 'E-mail harus diisi!',
            'email.unique' => 'E-mail tidak tersedia!',
            'password.required' => 'Password harus diisi!',
            'role.required' => 'Role harus dipilih!'            
        ]);
        
        User::create([
            'name' => Str::lower($request->name),
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => $request->role
        ]);

        return redirect('/user')->with('toast_success','Data user telah ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::all();
        $show = User::where('id',$id)->first();
        return view('user.show',compact('show','user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::all();
        $edit = User::find($id);
        return view('user.edit',compact('user','edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $request->validate([
            'name' => 'required|alpha_dash|unique:users,name,'.$user->id,
            'email' => 'required|unique:users,email,'.$user->id,
            'role' => 'required'
        ],
        [
            'name.required' => 'Username harus diisi!',
            'name.alpha_dash' => 'Username mengandung simbol yang dilarang',
            'name.unique' => 'Username tidak tersedia!',
            'email.required' => 'E-mail harus diisi!',
            'email.unique' => 'E-mail tidak tersedia!',
            'role.required' => 'Role harus dipilih!'            
        ]);

        $user = User::find($id);
        $user->name = Str::lower($request->name);
        $user->email = $request->email;
        $user->role = $request->role;
        $user->save();

        return redirect('/user')->with('toast_success','Data user telah diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect('/user');
    }
}
