<?php

namespace App\Http\Controllers;

use App\Models\master_barang;
use App\Models\transaksi_pembelian_barang;
use Illuminate\Http\Request;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
        $barang = master_barang::paginate(5);
        return view('produk.index',compact('barang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'nama_barang' => 'required',
            'harga_satuan' => 'required'
        ],
        [
            'nama_barang.required' => 'Masukkan nama barang!',
            'harga_satuan.required' => 'Masukkan harga barang!'
        ]);

        master_barang::create([
            'nama_barang' => $request->nama_barang,
            'harga_satuan' => $request->harga_satuan
        ]);

        return redirect('/produk')->with('toast_success','Data produk telah ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $barang = master_barang::paginate(5);
        $show = master_barang::where('id',$id)->first();
        $count = transaksi_pembelian_barang::where('master_barang_id',$id)->sum('jumlah');
        return view('produk.show',compact('barang','show','count'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $barang = master_barang::paginate(5);
        $edit = master_barang::find($id);
        return view('produk.edit',compact('barang','edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_barang' => 'required',
            'harga_satuan' => 'required'
        ],
        [
            'nama_barang.required' => 'Masukkan nama barang!',
            'harga_satuan.required' => 'Masukkan harga barang!'
        ]);

        $barang = master_barang::find($id);
        $barang->nama_barang = $request->nama_barang;
        $barang->harga_satuan = $request->harga_satuan;
        $barang->save();

        return redirect('/produk')->with('toast_success','Data produk telah diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        master_barang::where('id',$id)->delete();
        return redirect('produk');
    }

    public function search(Request $request){
        
        // dd($request);
        if ($request->filter_nama_barang != NULL) {
            if ($request->filter_harga != NULL) {
                $request->validate([
                    'filter_harga_min' => 'max:0',
                    'filter_harga_max' => 'max:0'
                ],
                [
                    'filter_harga_min.max' => 'Input box harga telah dimasukkan',
                    'filter_harga_max.max' => 'Input box harga telah dimasukkan'
                ]);
                $search =  master_barang::where('harga_satuan','=' , $request->filter_harga)
                ->where('nama_barang', 'LIKE' , '%' . $request->filter_nama_barang . '%')->get();
            } else {
                if ($request->filter_harga_min != NULL) {
                    if($request->filter_harga_max != NULL){
                        $request->validate([
                            'filter_harga_max' => 'gt:filter_harga_min'
                        ],[
                            'filter_harga_max.gt' => 'Harga max harus lebih besar dari harga min!'
                        ]);
                        $search = master_barang::where('nama_barang', 'LIKE' , '%' . $request->filter_nama_barang . '%')
                        ->where('harga_satuan' ,'>=', $request->filter_harga_min)
                        ->where('harga_satuan' ,'<=', $request->filter_harga_max)
                        ->get();
                    }else{
                        $search = master_barang::where('nama_barang', 'LIKE' , '%' . $request->filter_nama_barang . '%')
                        ->where('harga_satuan' ,'>=', $request->filter_harga_min)->get();
                    }
                    // $search = master_barang::where('nama_barang', 'LIKE' , '%' . $request->filter_nama_barang . '%')
                    // ->where('harga_satuan' ,'>', $request->filter_harga_min)->get();
                }else{
                    if($request->filter_harga_max != NULL){
                        $search = master_barang::where('nama_barang', 'LIKE' , '%' . $request->filter_nama_barang . '%')
                        ->where('harga_satuan' ,'<=', $request->filter_harga_max)
                        ->get();
                    }else{
                        $search = master_barang::where('nama_barang', 'LIKE' , '%' . $request->filter_nama_barang . '%')->get();
                    }
                }
            }
        }else{
            if ($request->filter_harga != NULL){
                $request->validate([
                    'filter_harga_min' => 'max:0',
                    'filter_harga_max' => 'max:0'
                ],
                [
                    'filter_harga_min.max' => 'Input box harga telah dimasukkan',
                    'filter_harga_max.max' => 'Input box harga telah dimasukkan'
                ]);
                $search = master_barang::where('harga_satuan',$request->filter_harga)->get();
            }else{
                if($request->filter_harga_min != NULL){
                    if($request->filter_harga_max != NULL){
                        $request->validate([
                            'filter_harga_max' => 'gt:filter_harga_min'
                        ],[
                            'filter_harga_max.gt' => 'Harga max harus lebih besar dari harga min!'
                        ]);
                        $search = master_barang::where('harga_satuan','>=', $request->filter_harga_min)
                        ->where('harga_satuan','<=',$request->filter_harga_max)->get();
                    }else{
                        $search = master_barang::where('harga_satuan','>=', $request->filter_harga_min)->get();
                    }
                }else{
                    if($request->filter_harga_max != NULL){
                        $search = master_barang::where('harga_satuan','<=',$request->filter_harga_max)->get();
                    }else{
                        $search = master_barang::all();
                    }
                }
            }
            
        }
        return view('produk.search',compact('search'));
    }
}
