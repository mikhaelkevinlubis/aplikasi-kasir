<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index(){
        return view('auth.login');
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required|email:dns',
            'password' => 'required'
        ],
        [
            'email.required' => 'Email/password salah!',
            'email.email' => 'Format email salah!',
            'password.required' => 'Email/password salah!'
        ]);    

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            return redirect()->intended('/transaksi')->with('toast_info','Hi,'. Auth::user()->name.' !');
        }

        return back()->with('loginError' ,' Login gagal!');
    }

    public function logout(Request $request){
        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();
        
        return redirect('/login');
    }
}
