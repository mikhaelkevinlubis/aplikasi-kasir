<?php

namespace App\Http\Controllers;

use App\Models\master_barang;
use App\Models\transaksi_pembelian;
use App\Models\transaksi_pembelian_barang;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

use function PHPUnit\Framework\callback;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        // 

        $now = Carbon::now()->format('Y-m-d');
        $transaksi_kasir = transaksi_pembelian::where('created_at', 'LIKE' , $now . '%')->get();
        $transaksi = transaksi_pembelian::get();
        $barang = master_barang::all();
        return view('transaksi.index',compact('transaksi','transaksi_kasir','barang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id = transaksi_pembelian::all()->max('id');
        $barang = master_barang::all();
        return view('transaksi.create',compact('id','barang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $request->validate([
            'total_harga' => 'required',
        ],
        [
            "total_harga.required" => "Silahkan pilih barang dan masukkan jumlah barang",
        ]);
        switch($request->input('action')){
            case 'save':
                $transaksiPembelianId = $request->transaksi_pembelian_id;
                $masterBarangId = $request->master_barang_id;
                $jumlah = $request->jumlah;
                $subTotal = $request->sub_total;
                $totalHarga = $request->total_harga;
                
                $transaksiPembelian = new transaksi_pembelian;
                $transaksiPembelian->total_harga = $totalHarga;
                $transaksiPembelian->save();
        
                for ($i=0; $i < count($masterBarangId); $i++) { 
                    $transaksiPembelianBarang = new transaksi_pembelian_barang;
                    $transaksiPembelianBarang->transaksi_pembelian_id = $transaksiPembelianId[0];
                    $transaksiPembelianBarang->master_barang_id = $masterBarangId[$i];
                    $transaksiPembelianBarang->jumlah = $jumlah[$i];
                    $transaksiPembelianBarang->sub_total = $subTotal[$i];
                    $hargaSatuan = master_barang::find($masterBarangId[$i]);
                    $transaksiPembelianBarang->harga_satuan = $hargaSatuan->harga_satuan;
                    $transaksiPembelianBarang->save();
                }
                
                return redirect('/transaksi')->with('toast_success','Transaksi berhasil ditambahkan!');
                break;
            
            case 'print':
                $id = $request->transaksi_pembelian_id;
                $qty = $request->jumlah;
                $subTotal = $request->sub_total;
                $tanggal = Carbon::now();
                $totalHarga = $request->total_harga;
                $masterBarangId = $request->master_barang_id;
                for ($i=0; $i <count($masterBarangId) ; $i++) { 
                    $cariNama = master_barang::find($masterBarangId[$i]);
                    $namaBarang[$i] =  $cariNama->nama_barang;
                }
                $pdf = PDF::loadview('transaksi.cetak_transaksi_pdf', ['id' => $id, 'qty' => $qty , 'subTotal' => $subTotal,'totalHarga' =>$totalHarga,'namaBarang' => $namaBarang, 'tanggal' => $tanggal])->setPaper('A8', 'portrait');
                return $pdf->stream();
                 

                break;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaksi = transaksi_pembelian::paginate(5);
        $nama_barang = transaksi_pembelian_barang::where('transaksi_pembelian_id',$id)->get();
        $transaksi_pembelian = transaksi_pembelian::find($id);
        return view('transaksi.show',compact('transaksi','nama_barang','transaksi_pembelian'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
       //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        transaksi_pembelian::where('id',$id)->delete();
        return redirect('/transaksi');
    }

    public function cetakDetailTransaksi($id)
    {
        $nama_barang = transaksi_pembelian_barang::where('transaksi_pembelian_id',$id)->get();
        $transaksi_pembelian = transaksi_pembelian::find($id);
        $pdf = PDF::loadview('transaksi.detail_transaksi_pdf', ['nama_barang' => $nama_barang, 'transaksi_pembelian' => $transaksi_pembelian]);
        return $pdf->stream();
    }

    public function search(Request $request){

        // $a = transaksi_pembelian::whereHas('transaksi_pembelian_barangs',function($x){
        //     //     $x->where('master_barang_id',$request->filter_barang_1);
        //     // })->get();
        
        $filterTanggal = $request->filter_tanggal;
        $filterMin = $request->filter_harga_min;
        $filterMax = $request->filter_harga_max;
        
        if($filterTanggal != NULL){
            if($filterMin != NULL){
                if($filterMax != NULL){
                    $request->validate([
                        'filter_harga_max' => 'gt:filter_harga_min'
                    ],
                    [
                        'filter_harga_max.gt' => 'Harga max harus lebih besar dari harga min'
                    ]);

                    $search = transaksi_pembelian::where('created_at','LIKE', $filterTanggal . '%')
                            ->where('total_harga' , '>=' , $filterMin)
                            ->where('total_harga', '<=' , $filterMax)
                            ->get();
                }else{
                    $search = transaksi_pembelian::where('created_at','LIKE', $filterTanggal . '%')
                            ->where('total_harga' , '>=' , $filterMin)
                            ->get();
                }
            }else{
                if($filterMax != NULL){
                    $search = transaksi_pembelian::where('created_at','LIKE', $filterTanggal . '%')
                            ->where('total_harga', '<=' , $filterMax)
                            ->get();
                }else{
                    $search = transaksi_pembelian::where('created_at','LIKE', $filterTanggal . '%')->get();
                }
            }
        }else{
            if($filterMin != NULL){
                if($filterMax != NULL){
                    $request->validate([
                        'filter_harga_max' => 'gt:filter_harga_min'
                    ],
                    [
                        'filter_harga_max.gt' => 'Harga max harus lebih besar dari harga min'
                    ]);

                    $search = transaksi_pembelian::where('total_harga','>=',$filterMin)
                            ->where('total_harga','<=',$filterMax)
                            ->get();
                }else{
                    $search = transaksi_pembelian::where('total_harga','>=', $filterMin)->get();
                }
            }else{
                if($filterMax != NULL){
                    $search = transaksi_pembelian::where('total_harga','<=',$filterMax)->get();
                }else{
                    $search = transaksi_pembelian::all();
                }
            }
        }

        // dd($request);
        return view('transaksi.search',compact('search'));
        
    }
}
