<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeyToTransaksiPembelianBarangs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaksi_pembelian_barangs', function (Blueprint $table) {
            $table->unsignedBigInteger('transaksi_pembelian_id');
            $table->foreign('transaksi_pembelian_id')->references('id')->on('transaksi_pembelians')->onDelete('cascade');
            $table->unsignedBigInteger('master_barang_id');
            $table->foreign('master_barang_id')->references('id')->on('master_barangs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaksi_pembelian_barangs', function (Blueprint $table) {
            $table->dropForeign(['master_barang_id']);
            $table->dropColumn(['master_barang_id']);
            $table->dropForeign(['transaksi_pembelian_id']);
            $table->dropColumn(['transaksi_pembelian_id']);
        });
    }
}
